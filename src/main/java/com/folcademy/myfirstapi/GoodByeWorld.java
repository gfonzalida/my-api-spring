package com.folcademy.myfirstapi;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GoodByeWorld {

    @PostMapping("/goodbye")
    public String googbye(){
        return "Goodbye World";
    }
}
